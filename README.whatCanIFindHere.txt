Directories estructure:
- SourceView: Source files to test CodeMirror: large files, binaries, etc 
|_ SourceView/SyntaxHighlighting: Files for every type supported by CodeMirror: http://codemirror.net/mode/index.html
- 1000_files: 1000 files


Branches:
- SyntaxHighlighting_ForDiffReview : Changes on every file to review Syntax HL for diff view

### PLEASE IF YOU READ THIS AND WANT TO ADD MORE DOCUMENTATION ABOUT THE TEST CONTENTS, FEEL FREE!

Markdown test cases

# h1
## h2
### h3
#### h4
##### h5
###### h6
####### still h6

This is an H1
=============

This is an H2
-------------


# This is an H1 #

## This is an H2 ##

### This is an H3 ######

#### This is an H4 ####

##### This is an H5 #####

###### This is an H6 ######
####### This is an H6 still #######



Mentions @admin @projectadmin @non existent user @"<script>alert(321)</script>"

http://github.github.com/github-flavored-markdown/ – this should be a link if we support autlolinking

Code block using 4 spaces
    public void stuff(){
      System.out.print('hey!');
    }


First line
Second line

Fourth line with empty line above

perform_complicated_task should not be italized

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

This is [an example](http://example.com/ "Title") inline link.

[This link](http://example.net/) has no title attribute.


*single asterisks*

_single underscores_

**double asterisks**

__double underscores__

un*frigging*believable


*   Abacus
    * answer
*   Bubbles
    1.  bunk
    2.  bupkis
        * BELITTLER
    3. burper
*   Cunning


> Email-style angle brackets
> are used for blockquotes.

> > And, they can be nested.

> #### Headers in blockquotes
> 
> * You can quote a list.
> * Etc.


`<code>` spans are delimited
by backticks.

You can include literal backticks
like `` `this` ``.


![Alt text](/path/to/img.jpg)

![Alt text](/s/en_US-1988229788/30af178/1/2.7.4/_/download/resources/com.atlassian.upm.atlassian-universal-plugin-manager-plugin%3Aupm-web-resources/images/marketplace-icon-sprite.png "Relative path image")

![Alt text](http://www.pictureup.net/data/media/8/1_images.jpg "Absolute path image")




*****



<!-- Simple table -->
<table>
  <tr>
    <td>John</td>
    <td>Doe</td>
  </tr>
  <tr>
    <td>Jane</td>
    <td>Doe</td>
  </tr>
</table>

<!-- Simple table with header -->
<table>
  <tr>
    <th>First name</th>
    <th>Last name</th>
  </tr>
  <tr>
    <td>John</td>
    <td>Doe</td>
  </tr>
  <tr>
    <td>Jane</td>
    <td>Doe</td>
  </tr>
</table>

<!-- Table with thead, tfoot, and tbody -->
<table>
  <thead>
    <tr>
      <th>Header content 1</th>
      <th>Header content 2</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td>Footer content 1</td>
      <td>Footer content 2</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <td>Body content 1</td>
      <td>Body content 2</td>
    </tr>
  </tbody>
</table>

<!-- Table with colgroup -->
<table>
  <colgroup span="4" class="columns"></colgroup>
  <tr>
    <th>Countries</th>
    <th>Capitals</th>
    <th>Population</th>
    <th>Language</th>
  </tr>
  <tr>
    <td>USA</td>
    <td>Washington D.C.</td>
    <td>309 million</td>
    <td>English</td>
  </tr>
  <tr>
    <td>Sweden</td>
    <td>Stockholm</td>
    <td>9 million</td>
    <td>Swedish</td>
  </tr>
</table>

<!-- Table with colgroup and col -->
<table>
  <colgroup>
    <col class="column1">
    <col class="columns2plus3" span="2">
  </colgroup>
  <tr>
    <th>Lime</th>
    <th>Lemon</th>
    <th>Orange</th>
  </tr>
  <tr>
    <td>Green</td>
    <td>Yellow</td>
    <td>Orange</td>
  </tr>
</table>

<!-- Simple table with caption -->
<table>
  <caption>Awesome caption</caption>
  <tr>
    <td>Awesome data</td>
  </tr>
</table>

* * *
