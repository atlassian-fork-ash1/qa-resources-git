# Assignment:
values   = 2
opp = false

# Conditions:
values = -2 if opp

# Arrays:
listofNumbers = [1, 2, 3, 4, 5]

# Objects:
math =
  root:   Math.sqrt
  square: square
  cube:   (x) -> x * square x

