package com.atlassian.stash.internal.branch.web

import com.atlassian.integration.jira.{JiraIssueUrlsRequest, JiraService}
import com.atlassian.soy.renderer.{SoyException, SoyTemplateRenderer}
import com.atlassian.stash.branch.model.BranchModelService
import com.atlassian.stash.exception.{NoDefaultBranchException, AuthorisationException}
import com.atlassian.stash.i18n.I18nService
import com.atlassian.stash.internal.branch.model.{BranchTypes, InternalBranchType}
import com.atlassian.stash.internal.branch.model.rest.RestBranchType
import com.atlassian.stash.repository._
import com.atlassian.stash.user.{Permission, PermissionService, PermissionValidationService, RecentlyAccessedRepositoriesService}
import com.atlassian.stash.util.PageRequestImpl
import com.atlassian.webresource.api.assembler.PageBuilderService

import javax.servlet.ServletException
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import java.io.IOException
import javax.annotation.Nonnull

import scala.collection.JavaConverters._

object BranchCreationServlet {
  val RESOURCE_KEY = "com.atlassian.stash.stash-branch-utils:server-side-templates"
  val BRANCH_CREATION_TEMPLATE_KEY = "stash.page.branchCreation"
  val NO_ACCESS_TEMPLATE_KEY = "stash.page.branchCreationNoWriteAccess"
  val BRANCH_CREATION_CONTEXT = "stash.page.branch.creation"
  val NO_WRITE_ACCESS_CONTEXT = "stash.page.branch.creation.noaccess"
}

/**
 * @since 2.7
 */
class BranchCreationServlet(
                             soyTemplateRenderer: SoyTemplateRenderer,
                             permissionService: PermissionService,
                             pageBuilderService: PageBuilderService,
                             permissionValidationService: PermissionValidationService,
                             branchCreationData: BranchCreationData
                             ) extends HttpServlet {

  import BranchCreationServlet._

  protected[web] override def doGet(httpRequest: HttpServletRequest, response: HttpServletResponse) {
    permissionValidationService.validateAuthenticated()
    response.setContentType("text/html;charset=UTF-8")
    if (!permissionService.hasAnyUserPermission(Permission.REPO_WRITE)) {
      pageBuilderService.assembler.resources.requireContext(NO_WRITE_ACCESS_CONTEXT)
      render(response, RESOURCE_KEY, NO_ACCESS_TEMPLATE_KEY, Map[String, AnyRef]())
    } else {
      pageBuilderService.assembler.resources.requireContext(BRANCH_CREATION_CONTEXT)
      val request = (param: String) => Option(httpRequest.getParameter(param))
      render(response, RESOURCE_KEY, BRANCH_CREATION_TEMPLATE_KEY, branchCreationData.soyResults(request))
    }
  }

  private def render(resp: HttpServletResponse, resourceKey: String, templateName: String, params: Map[String, AnyRef]) {
    try {
      soyTemplateRenderer.render(resp.getWriter, resourceKey, templateName, params.asJava)
    } catch {
      case e: SoyException => {
        e.getCause match {
          case cause: IOException => throw cause
          case _ => throw new ServletException(e)
        }
      }
    }
  }

}

object BranchCreationData {

  val ISSUE_TYPE_BUG = "Bug"
  val ISSUE_TYPE_STORY = "Story"
  val ISSUE_TYPE_NEW_FEATURE = "New Feature"
  val PARAM_REPO_ID = "repoId"
  val PARAM_BRANCH_TYPE = "branchType"
  val PARAM_ISSUE_KEY = "issueKey"
  val PARAM_ISSUE_TYPE = "issueType"
  val PARAM_ISSUE_SUMMARY = "issueSummary"
  val PARAM_BRANCH_FROM = "branchFrom"
  val PARAM_BRANCH_NAME = "branchName"
  val RECENT_REPO_PAGE_SIZE = 5
  val ISSUE_BRANCH_NAME_LIMIT = 40
}

class BranchCreationData(
                          permissionValidationService: PermissionValidationService,
                          repositoryService: RepositoryService,
                          recentlyAccessedRepositoriesService: RecentlyAccessedRepositoriesService,
                          repositoryMetadataService: RepositoryMetadataService,
                          i18nService: I18nService,
                          branchModelService: BranchModelService,
                          jiraService: JiraService
                          ) {

  import BranchCreationData._

  def soyResults(request: SoyData.Params): Map[String, AnyRef] = {
    // Build up list of SoyResult
    val recentRepositories = recentlyAccessedRepositoriesService.findByCurrentUser(
      Permission.REPO_WRITE,
      new PageRequestImpl(0, RECENT_REPO_PAGE_SIZE)
    ).getValues.asScala

    // Only use recent repositories if no repoId is specified
    val repository = request(PARAM_REPO_ID)
      .map(getRepositoryFromRequest)
      .getOrElse(recentRepositories.headOption)

    val repoData = repository.map {
      repo => {
        val isRepoEmpty = repositoryMetadataService.isEmpty(repo)
        val branchData =
          if (!isRepoEmpty) branchFrom(repo) |+| branchType(repo)
          else SoyData.zero
        repositoryDetails(repo, isRepoEmpty) |+| branchData
      }
    }.getOrElse(SoyData.zero)

    val result = repoData |+|
      issueKey(repository) |+|
      recentRepositoriesResult(recentRepositories) |+|
      branchName

    result.run(request).toMap
  }

  private def getRepositoryFromRequest(repoIdString: String): Option[Repository] = for {
    repoId <- try Some(Integer.parseInt(repoIdString)) catch {
      case e: NumberFormatException => None
    }
    repository <- Option(repositoryService.getById(repoId))
    _ <- try {
      permissionValidationService.validateForRepository(repository, Permission.REPO_WRITE)
      Some(repository)
    } catch {
      case e: AuthorisationException => None
    }
  } yield repository

  private def repositoryDetails(repository: Repository, repositoryEmpty: Boolean) = SoyData(params =>
    params(PARAM_REPO_ID)
      .flatMap(_ => Some('cancelRepository, repository))
      ++ Some('repository, repository)
      ++ Some('repositoryEmpty, Boolean.box(repositoryEmpty))
  )

  private def recentRepositoriesResult(recentRepos: Iterable[Repository]) = SoyData(_ =>
    Some('recentRepositories, recentRepos.asJava)
  )

  private def branchType(repository: Repository) = SoyData(params =>
    catchNoDefaultBranch(branchModelService.getModel(repository)).map(_.getTypes.asScala.toList :+ new CustomBranchType)
      .toIterable.flatMap {
      branchTypes =>
        val branchType = params(PARAM_BRANCH_TYPE).orElse(params(PARAM_ISSUE_TYPE).flatMap(issueTypeParam =>
          if (ISSUE_TYPE_BUG.equalsIgnoreCase(issueTypeParam)) {
            Some(BranchTypes.BUGFIX.getId)
          } else if (ISSUE_TYPE_STORY.equalsIgnoreCase(issueTypeParam) || ISSUE_TYPE_NEW_FEATURE.equalsIgnoreCase(issueTypeParam)) {
            Some(BranchTypes.FEATURE.getId)
          } else {
            None
          }
        )).flatMap(param => branchTypes.find(_.getId.equalsIgnoreCase(param)))
          .orElse(branchTypes.headOption)
          .map(new RestBranchType(_))
          .flatMap(Some("branchType", _))
        branchType ++ Some("branchTypes", branchTypes.map(new RestBranchType(_)).asJava)
    }
  )

  private def branchName = SoyData(params =>
    params(PARAM_BRANCH_NAME).orElse {
      BranchNameFormatter.generateBranchNameFromIssue(params(PARAM_ISSUE_KEY), params(PARAM_ISSUE_SUMMARY), ISSUE_BRANCH_NAME_LIMIT)
    }.flatMap(Some("branchName", _))
  )

  private def branchFrom(repository: Repository) = SoyData(params =>
    params(PARAM_BRANCH_FROM)
      .flatMap(branchFrom => Option(repositoryMetadataService.resolveRef(repository, branchFrom)))
      .orElse(catchNoDefaultBranch(repositoryMetadataService.getDefaultBranch(repository)))
      .flatMap(ref => Some("sourceRef", toViewRef(ref).toMap))
  )

  private def toViewRef(ref: Ref) = {
    import RefType._
    val refType = ref match {
      case _: Branch => ViewRefType(BRANCH, i18nService.getMessage("stash.web.revisionref.branch.name"))
      case _: Tag => ViewRefType(TAG, i18nService.getMessage("stash.web.revisionref.tag.name"))
      case _ => ViewRefType(COMMIT, i18nService.getMessage("stash.web.revisionref.commit.name"))
    }
    ViewRef(ref.getId, ref.getDisplayId, false, refType, ref.getLatestChangeset)
  }

  private def catchNoDefaultBranch[A](callback: => A): Option[A] =
    try Option(callback) catch {
      case e: NoDefaultBranchException => None
    }

  private def issueKey(repository: Option[Repository]) = SoyData(params =>
    params(PARAM_ISSUE_KEY).toIterable.flatMap(issueKey =>
      Some("issueKey", issueKey) ++ (if (jiraService.isLinked) {
        val builder = new JiraIssueUrlsRequest.Builder().issueKey(issueKey)
        val request = repository
          .map(repo => builder.entityKey(repo.getProject.getKey))
          .getOrElse(builder).build
        Some("issueUrl", jiraService.getIssueUrls(request).get(issueKey))
      }
      else None)
    )
  )

  private class CustomBranchType extends InternalBranchType {

    @Nonnull override def getPrefix = ""

    @Nonnull override def getId = "CUSTOM"

    @Nonnull override def getDisplayName = i18nService.getMessage("stash.branchmodel.type.CUSTOM")
  }

}
